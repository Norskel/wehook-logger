import json
import os
import logging

from datetime import datetime
from flask import Flask, request

STORE_PATH = os.getenv('STORE_PATH', '/webhook-logger/store')

app = Flask("webhook-logger")
logging.basicConfig(level=logging.DEBUG)

def store(data):
    dt = datetime.now().strftime("%Y-%m-%d-%Hh%M-%S-%f")[:-3]
    file_name = f"{STORE_PATH}/{dt}.json"
    # For gitlab webhook
    if 'project' in data['data']:
        file_name = f"{STORE_PATH}/{data['data']['project']['id']}---{dt}.json"
    logging.info(f"Store request respond in {file_name}")

    with open(file_name,"w") as f:
        json.dump(data,f,indent=2)
        f.close()

@app.route('/webhook', methods=['POST','GET'])
def webhook():
    data = {}
    data['method'] = request.method
    data["headers"] = {k:v for k, v in request.headers}
    data["data"] = request.json
    logging.info("New request !")
    logging.info(json.dumps(data, indent=2))
    store(data)
    return "", 200


if __name__ == '__main__':
    app.run(host='0.0.0.0')
