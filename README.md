# Wehook Logger

## Endpoint

    /webhook POST

## Variables

| Nom        | Description | Example               |
|:-----------|:------------|:----------------------|
| STORE_PATH |             | /webhook-logger/store |
