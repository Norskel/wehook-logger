FROM python:3.11-alpine

EXPOSE 5000

ENV STORE_PATH="/webhook-logger/store"

WORKDIR /webhook-logger
COPY ./requirements.txt /webhook-logger/requirements.txt
RUN pip install -r requirements.txt \
&& mkdir -p "$STORE_PATH"

COPY main.py /webhook-logger/

CMD ["python","main.py" ]
